import { ImportData } from "../../imports/api/importData";
import { DBConnect} from "../../imports/api/dbConnection";
import { UploadComplete } from "../../imports/api/uploadComplete";

Template.finalCheck.onCreated(function() {
    this.subscribe("importObject");
    this.subscribe("uploadSucceeded");
});

Template.finalCheck.onRendered(function() {
    
});

Template.finalCheck.helpers({
    pushCount: function() {
        return ImportData.find({}).count();
    },
    uploadComp: function() {
        return UploadComplete.findOne({});
    }
});

Template.finalCheck.events({
    'click #pushData' (event) {
        event.preventDefault();
        Meteor.call('saveDataToMariaDB', function(err, results) {
            if (err) {
                console.log("Error saving to db: " + err);
            } else {
                console.log("Data saved to db successfully.");
                console.log(results);
            }
        });
    },
    'click #backToFixTitles' (event) {
        event.preventDefault();
        FlowRouter.go('/fixTitles');
    },
    'click #pruneData' (event) {
        event.preventDefault();
        Meteor.call('removeTheData', function(err, result) {
            if (err) {
                console.log("Error deleting the data: " + err);
            } else {
                Meteor.call('uploadClear', function(error, results) {
                    if (error) {
                        console.log("Error removing the Upload Success flag.");
                    } else {
                        console.log("Success deleting data and removing the upload sucess flag.");
                    }
                });
            }
        });
    },
    'click #tryAgain' (event) {
        event.preventDefault();

    }
});