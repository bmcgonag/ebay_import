import { ImportData } from '../../imports/api/importData';

Template.fixTitles.onCreated(function() {
    this.subscribe("importObject");
});

Template.fixTitles.onRendered(function() {

});

Template.fixTitles.helpers({

});

Template.fixTitles.events({
    'click #fixDescBtn' (event) {        
        event.preventDefault();

        let data = ImportData.find().fetch();
        let noItems = data.length;
        for (i=0; i < noItems; i++) {
            let itemId = data[i]._id;
            let title = data[i].ItemTitle;
            let splitTitle = title.split('');
            let titleChnaged = false;

            let numWords = splitTitle.length;
            for (j=0; j < numWords; j++) {
                if (splitTitle[j] == '"') {
                    splitTitle[j] = ' in.';
                    titleChnaged = true;
                }
            }

            newTitle = splitTitle.join('');
            
            if (titleChnaged == true) {
                Meteor.call('update_itemTitle', itemId, newTitle, function(err, result) {
                    if (err) {
                        console.log("Error updating itemTitle: " + err);
                    } else {
                        console.log("    ----    ----    ----    ----    ----");
                        console.log(newTitle);
                    }
                });
            }
            if (i == (noItems - 1)) {
                FlowRouter.go('/finalCheck');
            } else {
                console.log("    = = = = = =    Never got to i = noItems.");
            }
        }
        console.log("    ====    Item Titles Changed.");


    },
    'click #cancelFixDescBtn' (event) {
        event.preventDefault();
        FlowRouter.go('/verifyData');
    },
});