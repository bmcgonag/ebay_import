import { DBConnect } from '../../imports/api/dbConnection';

Template.importWiz.onCreated(function() {
    this.subscribe("savedCreds");
});

Template.importWiz.onRendered(function() {

});

Template.importWiz.helpers({
    dbSetup: function () {
        let dbSet = DBConnect.findOne({});
        if (typeof dbSet == 'undefined' || dbSet == null || dbSet == null) {
            return false;
        } else {
            // console.log(dbSet.dbhost);
            return true;
        }
    },
});

Template.importWiz.events({
    'click #dbConnection' (event) {
        event.preventDefault();
        FlowRouter.go('/importDB');
    },
    'click #nextToImport' (event) {
        event.preventDefault();
        FlowRouter.go('/importFileSelect');
    }
});