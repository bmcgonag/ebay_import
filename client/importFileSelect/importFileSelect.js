import {ImportData} from '../../imports/api/importData';

Template.importFileSelect.onCreated(function() {
    this.subscribe("importObject");
});

Template.importFileSelect.onRendered(function() {
    Session.set("infoImported", false);
    Session.set("fileSelected", false);
});

Template.importFileSelect.helpers({
    infoImported: function() {
        return Session.get("infoImported");
    },
    fileSelected: function() {
        return Session.get("fileSelected");
    },
});

Template.importFileSelect.events({
    'change #ebayFile' (event) {
        var filename = $("#ebayFile").val();
        Session.set("fileSelected", true);
        // console.log("Filename is: " + filename);
        document.getElementById('customFileUpload').innerHTML = filename;
        document.getElementById("customFileUpload").className = "custom-file-selected";
    },
    'click #parseFileBtn' (event) {
        event.preventDefault();
        // console.log("Got Here!");
        let importFile = document.getElementById("ebayFile").files[0];
        // make sure we have a file
        if (importFile == "" || importFile == null) {
            console.log("Error: No File Detected!");
        } else {
            console.log("Made it to parsing.");
            Papa.parse(importFile, {
                delimiter: ",",
                header: true,
                dynamicTyping: false,
                complete: function(results) {
                    console.log("Waiting...");
                    console.dir(results);
                    Session.set("importResults", results);
                    Meteor.call("import_parsedData", results, function(err, result) {
                        if (err) {
                            console.log("Error calling the import API: " + err);
                            Session.set("infoImported", false);
                        } else {
                            // console.log("Import API Called Successfully! Result is: " + result);
                            FlowRouter.go('/verifyData');
                        }
                    });
                    // FlowRouter.go('/verifyData');
                },
                error: function(error) {
                    console.log("Error on parse: " + error);
                },
            });
        }

    },
    'click #showPrevImportedData' (event) {
        event.preventDefault();
        FlowRouter.go('/verifyData');
    },
    'click #cancelParse' (event) {
        event.preventDefault();
        FlowRouter.go('/importWiz');
    },
});