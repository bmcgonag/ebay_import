import { Meter } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const UploadComplete = new Mongo.Collection('upload_complete');

UploadComplete.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'uploadSuccess' (success) {
        check(success, Boolean);

        return UploadComplete.insert({
            success: success
        });
    },
    'uploadClear' () {
        return UploadComplete.remove({});
    },
});