FlowRouter.route('/', {
    name: 'home',
    action() {
        BlazeLayout.render('MainLayout', { main: "importWiz" });
    }
});

FlowRouter.route('/importWiz', {
    name: 'home',
    action() {
        BlazeLayout.render('MainLayout', { main: "importWiz" });
    }
});

FlowRouter.route('/importFileSelect', {
    name: 'importFileSelect',
    action() {
        BlazeLayout.render('MainLayout', { main: "importFileSelect" });
    }
});

FlowRouter.route('/fixTitles', {
    name: 'fixTitles',
    action() {
        BlazeLayout.render('MainLayout', { main: "fixTitles" });
    }
});

FlowRouter.route('/finalCheck', {
    name: 'finalCheck',
    action() {
        BlazeLayout.render('MainLayout', { main: "finalCheck" });
    }
});

FlowRouter.route('/importDb', {
    name: 'importDb',
    action() {
        BlazeLayout.render('MainLayout', { main: "importDb"});
    }
});

FlowRouter.route('/verifyData', {
    name: 'verifyData',
    action() {
        BlazeLayout.render('MainLayout', { main: "verifyData"});
    }
});

FlowRouter.route('/convertData', {
    name: 'convertData',
    action() {
        BlazeLayout.render('MainLayout', { main: "convertData"});
    }
});

FlowRouter.route('/removeData', {
    name: 'removeData',
    action() {
        BlazeLayout.render('MainLayout', { main: "removeData"});
    }
});