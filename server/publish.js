import { ImportData } from '../imports/api/importData';
import { DBConnect } from '../imports/api/dbConnection';
import { UploadComplete } from '../imports/api/uploadComplete';

Meteor.publish("importObject", function() {
    try {
        return ImportData.find({}, { limit: 250 });  
    } catch (error) {
        console.log("Error publishing importData: " + error);
    }
});

Meteor.publish('savedCreds', function() {
    try {
        return DBConnect.find({});
    } catch (error) {
        console.log("Error retrieving saved db credentials: " + error);
    }
});

Meteor.publish('uploadSucceeded', function() {
    try {
        return UploadComplete.find({});
    } catch (error) {
        console.log("Error retrieving uploadSuccessStatus: " + error);
    }
});