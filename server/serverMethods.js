import { Meter } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { DBConnect } from '../imports/api/dbConnection';
import { ImportData } from '../imports/api/importData';
import { UploadComplete } from '../imports/api/uploadComplete';
import mysql from 'mysql2';
Fibers = Npm.require('fibers')

Meteor.methods({
    'test.connection' () {
        let dbConn = DBConnect.findOne({});

        const connection = mysql.createConnection({
            host: dbConn.dbhost,
            user: dbConn.dbuser,
            password: dbConn.dbpass,
            database: dbConn.dbname
        });

        connection.query(
            "SELECT version();",
                function(err, results, fields) {
                    if (err) {
                        console.log("Error on DB Connection Test: " + err);
                    } else {
                        console.log("DB Connection Test Succeeded: ");
                        console.dir(results);
                    }
                }
        );
    },
    'saveDataToMariaDB' () {
        let dataToImport = ImportData.find({}).fetch();

        let numberFiles = dataToImport.length;

        let dbConn = DBConnect.findOne({});

        const connection = mysql.createConnection({
            host: dbConn.dbhost,
            user: dbConn.dbuser,
            password: dbConn.dbpass,
            database: dbConn.dbname
        });

        let trackingNo;

        for (i = 0; i < numberFiles; i++) {
            
            if (dataToImport[i].TrackingNumber == "") {
                trackingNo = null;
            } else {
                trackingNo = dataToImport[i].TrackingNumber;
            }

            connection.query(
                'INSERT INTO ebay_info (`Sales Record Number`, `Order Number`, `Buyer Username`, `Buyer Name`, `Buyer Email`, `Buyer Note`, `Buyer Address 1`, `Buyer Address 2`, `Buyer City`, `Buyer State`, `Buyer Zip`, `Buyer Country`, `Ship To Name`, `Ship To Phone`, `Ship To Address 1`, `Ship To Address 2`, `Ship To City`, `Ship To State`, `Ship To Zip`, `Ship To Country`, `Item Number`, `Item Title`, `Size`, `Custom Label`, `Sold Via Promoted Listings`, `Quantity`, `SoldFor`, `ShippingAndHandling`, `SalesCollectedTax`, `EbayCollectedTax`, `TotalPrice`, `Payment Method`, `Sale Date`, `Year`, `SaleDayName`, `SaleMonthName`, `Paid On Date`, `Ship By Date`, `Minimum Estimated Delivery Date`, `Maximum Estimated Delivery Date`, `Shipped On Date`, `Feedback Left`, `Feedback Received`, `My Item Note`, `PayPal Transaction ID`, `Shipping Service`, `Tracking Number`, `Transaction ID`, `Variation Details`, `Global Shipping Program`, `Global Shipping Reference ID`, `Click And Collect`, `Click And Collect Reference Number`, `eBay Plus`, `eBay Collected Tax and Fees Included in Total`) VALUES ("' + dataToImport[i].SalesRecordNumber + '", "' + dataToImport[i].OrderNumber + '","' + dataToImport[i].BuyerUsername + '", "' + dataToImport[i].BuyerName + '", "' + dataToImport[i].BuyerEmail + '", "' + dataToImport[i].BuyerNote + '", "' + dataToImport[i].BuyerAddress1 + '", "' + dataToImport[i].BuyerAddress2 + '", "' + dataToImport[i].BuyerCity + '", "' + dataToImport[i].BuyerState + '", "' + dataToImport[i].BuyerZip + '", "' + dataToImport[i].BuyerCounty + '", "' + dataToImport[i].ShipToName + '", "' + dataToImport[i].ShipToPhone + '", "' + dataToImport[i].ShiptToAddress1 + '", "' + dataToImport[i].ShipToAddress2 + '", "' + dataToImport[i].ShipToCity + '", "' + dataToImport[i].ShipToState + '", "' + dataToImport[i].ShipToZip + '", "' + dataToImport[i].ShipToCountry + '", ' + dataToImport[i].ItemNumber + ', "' + dataToImport[i].ItemTitle + '", "' + dataToImport[i].Size + '", "' + dataToImport[i].CustomLabel + '", "' + dataToImport[i].SoldViaPromotedListings + '", ' + dataToImport[i].Quantity + ', ' + dataToImport[i].SoldFor + ', ' + dataToImport[i].ShippingAndHandling + ', ' + dataToImport[i].SellerCollectedTax + ', ' + dataToImport[i].eBayCollectedTax + ', ' + dataToImport[i].TotalPrice + ', "' + dataToImport[i].PaymentMethod + '", "' + dataToImport[i].SaleDate + '", "' + dataToImport[i].Year + '", "' + dataToImport[i].SaleDayName + '", "' + dataToImport[i].SaleMonthName + '", "' + dataToImport[i].PaidOnDate + '", "' + dataToImport[i].ShipByDate + '", "' + dataToImport[i].MinimumEstimatedDeliveryDate + '", "' + dataToImport[i].MaximumEstimatedDeliveryDate + '", "' + dataToImport[i].ShippedOnDate + '", "' + dataToImport[i].FeedbackLeft + '", "' + dataToImport[i].FeedbackReceived + '", "' + dataToImport[i].MyItemNote + '", "' + dataToImport[i].PayPalTransactionID + '", "' + dataToImport[i].ShippingService + '", ' + trackingNo + ', "' + dataToImport[i].TransactionID + '", "' + dataToImport[i].VariationDetails + '", "' + dataToImport[i].GlobalShippingProgam + '", "' + dataToImport[i].GlobalShippingReferenceID + '", "' + dataToImport[i].ClickAndCollect + '", "' + dataToImport[i].ClickAndCollectReferenceNumber + '", "' + dataToImport[i].eBayPlus + '", "' + dataToImport[i].eBayCollectedTaxesAndFeesIncludedInTotal + '")',
                function(err, results, fields) {
                    if (err) {
                        console.log("Error uploading to mariadb: " + err);
                    } else {
                        // console.log(results);
                        // console.log(fields);
                    } 
                }
            );

            if (i == (numberFiles - 1)) {
                // go get the count of the items in the db from this month and year, and compare
                // it to the count of items in the mongo db, and if they match give a success MessageChannel
                connection.query(
                    'SELECT COUNT(*) AS "dbRows" FROM ebay_info WHERE `Year` = "' + dataToImport[2].Year + '" AND `SaleMonthName` = "' + dataToImport[2].SaleMonthName +'";',
                    Meteor.bindEnvironment(function(err, results, fields) {
                        if (err) {
                            console.log("Error retrieving imported count: " + err);
                        } else {
                            console.log("Count retrieved, standby for match check.");
                            console.log(results[0].dbRows);
                            if (numberFiles == results[0].dbRows) {
                                Meteor.call('uploadSuccess', true);
                            } else {
                                Meteor.call('uploadSuccess', false);
                            }
                        }
                    })
                )
            }
        }
    },
});